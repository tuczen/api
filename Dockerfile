FROM php:8.0-fpm-alpine3.16

ARG ENV

RUN apk add libpq-dev && docker-php-ext-install pdo_pgsql

RUN if [ "$ENV" = "local" ]; then \
    apk add --no-cache $PHPIZE_DEPS \
    && pecl install xdebug \
    && docker-php-ext-enable xdebug \
    && deluser www-data \
    && adduser -D -s /bin/ash -u 1000 www-data; \
    fi

COPY ./etc/xdebug/xdebug.ini /usr/local/etc/php/conf.d/xdebug.ini
COPY --from=composer /usr/bin/composer /usr/bin/composer

RUN mkdir -p /app

WORKDIR /app

COPY . .

RUN if [ "$ENV" != "local" ]; then \
    composer install --no-dev --no-interaction --no-scripts; \
    fi

RUN chown -R www-data:www-data /app/storage
