<?php

namespace Database\Factories;

use App\Models\Group;
use App\Models\Sender;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->name,
            'email' => $this->faker->unique()->safeEmail,
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
            'is_dealer' => $this->faker->boolean
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return UserFactory
     */
    public function unverified(): UserFactory
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }

    /**
     * @return UserFactory
     */
    public function dealer(): UserFactory
    {
        return $this->state(function (array $attributes) {
            return [
                'is_dealer' => true,
            ];
        });
    }

    /**
     * @return UserFactory
     */
    public function sender(): UserFactory
    {
        return $this->state(function (array $attributes) {
            return [
                'is_dealer' => false,
            ];
        });
    }

    /**
     * @param  Group|null $group
     * @return UserFactory
     */
    public function attachGroup(?Group $group = null): UserFactory
    {
        $group = $group ?: Group::factory();

        return $this->hasAttached($group);
    }

    /**
     * @param  Sender|null $sender
     * @param  bool $asAdmin
     * @return UserFactory
     */
    public function attachSender(?Sender $sender = null, $asAdmin = false): UserFactory
    {
        $sender = $sender ?: Sender::factory();

        return $this->hasAttached($sender, ['is_admin' => $asAdmin]);
    }
}
