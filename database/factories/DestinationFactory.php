<?php


namespace Database\Factories;


use App\Models\Shipment;
use Illuminate\Database\Eloquent\Factories\Factory;

class DestinationFactory extends Factory
{
    public function definition()
    {
        $dateFrom = $this->faker->dateTimeBetween('+1 days', '+3 days');
        $dateTo = $this->faker->dateTimeBetween($dateFrom, (clone $dateFrom)->add(new \DateInterval('P2D')));

        return [
            'address' => $this->faker->address,
            'contact_name' => $this->faker->name,
            'contact_phone' => $this->faker->phoneNumber,
            'date_from' => $dateFrom->format('Y-m-d H:i'),
            'date_to' => $dateTo->format('Y-m-d H:i'),
            'amount' => $this->faker->randomFloat(2, 0, 10000),
            'is_pick_up' => $this->faker->boolean,
            'message' => $this->faker->text(255),
            'shipment_id' => Shipment::factory(),
        ];
    }

}
