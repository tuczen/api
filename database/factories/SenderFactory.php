<?php


namespace Database\Factories;


use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class SenderFactory extends Factory
{
    public function definition(): array
    {
        return [
            'name' => $this->faker->company
        ];
    }

    /**
     * @param  User|null $user
     * @param  array $pivot
     * @return SenderFactory
     */
    public function withUser(?User $user = null, array $pivot = []): SenderFactory
    {
        $user = $user ?: User::factory();

        return $this->hasAttached($user, $pivot);
    }

}
