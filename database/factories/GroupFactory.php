<?php


namespace Database\Factories;


use App\Models\Sender;
use Illuminate\Database\Eloquent\Factories\Factory;

class GroupFactory extends Factory
{
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'owner_id' => Sender::factory()
        ];
    }

}
