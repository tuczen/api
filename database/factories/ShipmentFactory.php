<?php


namespace Database\Factories;

use App\Models\Destination;
use App\Models\Group;
use App\Models\Sender;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ShipmentFactory extends Factory
{
    /**
     * @return array
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->city,
            'amount' => $this->faker->randomFloat(2, -5000, 5000),
            'creator_id' => User::factory()->sender(),
            'sender_id' => function (array $attributes) {
                return User::find($attributes['creator_id'])
                    ->senders()->first()->getKey();
            }
        ];
    }

    public function creator(User $user)
    {
        return $this->for($user, 'dealer');
    }

    /**
     * @return ShipmentFactory
     */
    public function assigned(): ShipmentFactory
    {
        return $this->state(function () {
            return [
                'dealer_id' => User::factory()->dealer(),
            ];
        });
    }

    /**
     * @return ShipmentFactory
     */
    public function confirmed(): ShipmentFactory
    {
        return $this->state(function (array $attributes) {
            return [
                'status' => 'confirmed',
            ];
        });
    }

    /**
     * @return ShipmentFactory
     */
    public function withDestinations(): ShipmentFactory
    {
        return $this->has(Destination::factory()->count(2));
    }

    /**
     * @param  Group|null $group
     * @return ShipmentFactory
     */
    public function withGroups(?Group $group = null): ShipmentFactory
    {
        return  $this->hasAttached($group ?: Group::factory()->count(2));
    }

    /**
     * @param  Sender|null $sender
     * @return ShipmentFactory
     */
    public function withSender(?Sender $sender = null): ShipmentFactory
    {
        $sender = $sender ?: Sender::factory();

        return $this->for($sender);
    }
}
