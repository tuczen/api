<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDestinationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('destinations', function (Blueprint $table) {
            $table->id();
            $table->string('address');
            $table->string('contact_name');
            $table->string('contact_phone');
            $table->dateTime('date_from');
            $table->dateTime('date_to');
            $table->decimal('amount');
            $table->boolean('is_pick_up')->default(false);
            $table->string('message')->nullable();
            $table->string('status')->default('pending');
            $table->unsignedBigInteger('shipment_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('destinations');
    }
}
