<?php


use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'oauth'], function () {

    Route::get('redirect', 'OAuthController@redirect');

    Route::get('callback', 'OAuthController@callback');
});

Route::middleware('auth:sanctum')
    ->prefix('user')
    ->group(function () {
        Route::get('/', 'UserController@user');

        Route::get('senders', 'UserController@senders');

        Route::put('device-token', 'UserController@setToken');
    });

Route::namespace('Senders')
    ->group(function () {

        Route::apiResource('senders', 'SenderController');

        Route::apiResource('senders.users', 'UserController')
            ->scoped();

        Route::apiResource('senders.groups', 'GroupController')
            ->scoped();

        Route::apiResource('senders.shipments', 'ShipmentController')
            ->scoped();

        Route::resource('groups.senders', 'GroupSenderController')
            ->only('store', 'destroy')
            ->scoped();

        Route::apiResource('groups.dealers', 'DealerController')
            ->except('update')
            ->scoped();

        Route::apiResource('shipments.destinations', 'DestinationController')
            ->scoped();

        Route::apiResource('shipments.groups', 'ShipmentGroupController')
            ->scoped();
    });


Route::namespace('Dealers')
    ->group(function () {

        Route::apiResource('available-shipments', 'AvailableShipmentController')
            ->only('index', 'update');

        Route::apiResource('assigned-shipments', 'ShipmentController')
            ->parameter('shipment', 'dealer_shipment')
            ->only('index', 'show');

        Route::apiResource('assigned-shipments.destinations', 'DestinationController')
            ->parameter('shipment', 'dealer_shipment')
            ->only('index', 'show', 'update');
    });
