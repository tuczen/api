<?php

namespace Tests;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Routing\Middleware\SubstituteBindings;
use Illuminate\Support\Str;
use Illuminate\Testing\Fluent\AssertableJson;
use Illuminate\Testing\TestResponse;
use Laravel\Sanctum\Sanctum;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, RefreshDatabase;

    /**
     * @param  string $uri
     * @return TestResponse
     */
    protected function get_protected_route_without_authentication(string $uri): TestResponse
    {
        return $this->json('GET', $uri)
            ->assertStatus(401)
            ->assertExactJson([
                'message' => 'Unauthenticated.'
            ]);
    }

    protected function resource_need_authentication(string $uri, array $except = ['any'])
    {
        $methods = collect([
            'index' => 'GET', 'show' => 'GET', 'update' => 'PUT',
            'store' => 'POST', 'delete' => 'DELETE'
        ])->except($except);

        foreach ($methods as $name => $method) {

            $uriParam = in_array($name, ['index', 'store']) ? '' : 'ID';

            $this->json($method, "$uri/$uriParam")//->dump()
                ->assertStatus(401)
                ->assertExactJson([
                    'message' => 'Unauthenticated.'
                ]);
        }
    }

    /**
     * @param  string $uri
     * @param  string $ability
     * @param  array|string[] $except
     */
    protected function resource_need_ability(string $uri, string $ability, array $except = ['any'])
    {
        $this->withoutMiddleware(SubstituteBindings::class);
        $this->setUser();

        $methods = collect([
            'index' => 'GET', 'show' => 'GET', 'update' => 'PUT',
            'store' => 'POST', 'delete' => 'DELETE'
        ])->except($except);

        foreach ($methods as $name => $method) {

            $uriParam = in_array($name, ['index', 'store']) ? '' : 'ID';

            $this->json($method, "$uri/$uriParam")//->dump()
                ->assertStatus(403)
                ->assertExactJson([
                    'message' => "Required ability [$ability] is missing."
                ]);
        }
    }

    /**
     * @param  string $uri
     * @param  int $count
     * @return TestResponse
     */
    protected function test_listing(string $uri, int $count): TestResponse
    {
        return $this->json('GET', $uri)//->dump()
            ->assertStatus(200)
            ->assertJsonCount($count, 'data')
            ->assertJsonStructure($this->jsonPaginationStructure());
    }

    /**
     * @param  string $method
     * @param  string $uri
     * @param  array $data
     * @return TestResponse
     */
    protected function test_unauthorized_action(string $method, string $uri, array $data = []): TestResponse
    {
        return $this->json($method, $uri, $data)
            ->assertStatus(403)
            ->assertJson(fn (AssertableJson $json) =>
            $json->where('message', 'This action is unauthorized.')
                ->etc()
            );
    }

    /**
     * @param  string $uri
     * @param  array $data
     * @return TestResponse
     */
    protected function test_creation(string $uri, array $data): TestResponse
    {
        return $this->json('POST', $uri, $data)
            ->assertStatus(201);
    }

    /**
     * @param  string $uri
     * @param  array $data
     * @return TestResponse
     */
    protected function test_update(string $uri, array $data): TestResponse
    {
        return $this->json('PUT', $uri, $data)
            ->assertStatus(200);
    }

    /**
     * @param  string $uri
     * @return TestResponse
     */
    protected function test_delete(string $uri): TestResponse
    {
        return $this->json('DELETE', $uri)
            ->assertStatus(200);
    }

    /**
     * @param  string $uri
     * @param  int $id
     * @return TestResponse
     */
    protected function test_show(string $uri, int $id): TestResponse
    {
        return $this->json('GET', $uri . (Str::endsWith($uri, $id) ? "" : "/$id"))
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) =>
                $json->where('id', $id)
                    ->etc()
            );
    }

    /**
     * @return string[]
     */
    public function jsonPaginationStructure(): array
    {
        return [
            'current_page',
            'data',
            'first_page_url',
            'from',
            'last_page',
            'last_page_url',
            'links',
            'next_page_url',
            'path',
            'per_page',
            'prev_page_url',
            'to',
            'total'
        ];
    }

    /**
     * @param  User|null $user
     * @param  array $abilities
     */
    public function setUser(?User $user = null, array $abilities = [])
    {
        $user = $user ?: User::factory()->make();

        Sanctum::actingAs($user, $abilities);
    }

    /**
     * @param  User|null $user
     */
    public function setSenderUser(?User $user = null)
    {
        $this->setUser($user, ['sender']);
    }

    /**
     * @param  User|null $user
     */
    public function setDealerUser(?User $user = null)
    {
        $this->setUser($user, ['dealer']);
    }
}
