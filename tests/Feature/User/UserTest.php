<?php


namespace Tests\Feature\User;


use App\Models\User;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class UserTest extends TestCase
{
    public function test_get_user_attributes()
    {
        $user = User::factory()->create();

        Sanctum::actingAs($user);

        $this->json('GET', 'api/user')
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) =>
                $json->has('id')
                    ->has('name')
                    ->has('email')
                    ->has('is_dealer')
            );
    }

    public function test_get_user_attributes_without_authentication()
    {
        $this->get_protected_route_without_authentication('api/user');
    }

    public function test_get_user_senders()
    {
        $user = User::factory()->create();

        Sanctum::actingAs($user);

        $this->json('GET', 'api/user/senders')
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) =>
                $json->has($user->senders()->count())
                    ->first(function ($json) {
                        $json->has('id')
                            ->has('name')
                            ->has('personal_account')
                            ->etc();
                    })
            );
    }

    public function test_get_user_senders_without_authentication()
    {
        $this->get_protected_route_without_authentication('api/user/senders');
    }
}
