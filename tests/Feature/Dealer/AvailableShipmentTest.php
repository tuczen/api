<?php


namespace Tests\Feature\Dealer;


use App\Models\Group;
use App\Models\Shipment;
use App\Models\User;
use Tests\TestCase;

class AvailableShipmentTest extends TestCase
{

    public function test_dealer_can_list_only_available_shipments()
    {
        $count = rand(3, 5);
        $this->createShipments($count);

        $this->test_listing("api/available-shipments", $count);
    }

    public function test_dealer_can_accept_shipment()
    {
        $shipment = last($this->createShipments(1));

        $this->test_update("api/available-shipments/{$shipment->getKey()}", []);
    }

    public function test_dealer_cant_accept_unavailable_shipment()
    {
        $shipment = last($this->createShipments(1));

        $this->setDealerUser();

        $this->json('PUT', "api/available-shipments/{$shipment->getKey()}")
            ->assertStatus(404);
    }

    /**
     * @param  int $count
     * @return array|Shipment[]
     */
    protected function createShipments(int $count): array
    {
        /** @var User $dealer */
        $dealer = User::factory()->dealer()->create();
        $shipments = [];
        // add extra group
        $count++;
        $c = 0;

        while ($c < $count) {

            // Group with for random sender
            $group = Group::factory()->create();

            // Shipment for sender owner of group
            $shipment = Shipment::factory()->confirmed()
                ->withSender($group->sender)
                ->withGroups($group)
                ->withDestinations()
                ->create();

            // Attach dealer to group, except to first
            if ($c > 0) {
                $dealer->groups()->attach($group->getKey());
                $shipments[] = $shipment;
            }

            $c++;
        }

        $this->setDealerUser($dealer);

        return $shipments;
    }
}
