<?php


namespace Tests\Feature;


use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Socialite\Facades\Socialite;
use Mockery;
use Tests\TestCase;

class OAuthTest extends TestCase
{
    public function test_redirect()
    {
        $userTypes = ['dealer', 'sender'];
        $data = [
            'user_type' => $userTypes[rand(0, 1)],
            'device_name' => uniqid()
        ];

        $this->json('GET', 'api/oauth/redirect', $data)
            ->assertStatus(302);
    }

    public function test_new_user_callback()
    {
        $user = User::factory()->make();
        $this->callbackTest($user);

        $this->assertDatabaseHas($user->getTable(), [
            'email' => $user->email,
            'is_dealer' => $user->is_dealer
        ]);
    }

    public function test_existing_user_callback()
    {
        $user = User::factory()->create();
        $this->callbackTest($user);
    }

    protected function callbackTest(User $user)
    {
        $abstractUser = Mockery::mock('Laravel\Socialite\Two\User');

        $abstractUser
            ->shouldReceive('getName')
            ->andReturn($user->name)
            ->shouldReceive('getEmail')
            ->andReturn($user->email);

        Socialite::shouldReceive('driver->stateless->user')
            ->andReturn($abstractUser);

        $state = json_encode([
            'user_type' => $user->is_dealer ? 'dealer' : 'sender',
            'device_name' => Str::camel($user->name)
        ]);

        $this->json('GET', 'api/oauth/callback', compact('state'))
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) =>
                $json->has('token')
            );
    }
}
