<?php


namespace Tests\Feature\Sender;


use App\Models\Sender;
use App\Models\User;
use Laravel\Sanctum\Sanctum;
use Tests\CreatesSender;
use Tests\TestCase;

class SenderTest extends TestCase
{
    use CreatesSender;

    public function test_resource_protection()
    {
        $this->resource_need_authentication("api/senders");
    }

    public function test_any_user_can_list()
    {
        $count = 3;
        Sender::factory()->count($count)->create();

        Sanctum::actingAs(User::factory()->sender()->make(), ['sender']);

        $this->test_listing("api/senders", $count);
    }

    public function test_any_user_can_show()
    {
        $sender = Sender::factory()->create();

        Sanctum::actingAs(User::factory()->sender()->make(), ['sender']);

        $this->test_show("api/senders", $sender->getKey());
    }

    public function test_any_user_can_create()
    {
        $sender = Sender::factory()->make();

        Sanctum::actingAs(User::factory()->sender()->make(), ['sender']);

        $this->test_creation("api/senders", $sender->toArray());
    }

    public function test_invalid_create()
    {
        Sanctum::actingAs(User::factory()->sender()->make(), ['sender']);

        $this->json('POST', "api/senders")
            ->assertJsonValidationErrors(['name']);
    }

    public function test_invalid_update()
    {
        $sender = $this->createSenderForRequest(true);

        $this->json('PUT', "api/senders/{$sender->getKey()}")
            ->assertJsonValidationErrors(['name']);
    }

    public function test_any_user_cant_update()
    {
        $sender = $this->createSenderForRequest(false);

        $this->test_unauthorized_action('PUT', "api/senders/{$sender->getKey()}");
    }

    public function test_admin_user_can_update()
    {
        $sender = $this->createSenderForRequest(true);

        $this->test_update("api/senders/{$sender->getKey()}", ['name' => __FUNCTION__]);
    }

    public function test_admin_user_can_delete()
    {
        $sender = $this->createSenderForRequest(true);

        $this->test_delete("api/senders/{$sender->getKey()}");
    }

    public function test_any_user_cant_delete()
    {
        $sender = $this->createSenderForRequest(false);

        $this->test_unauthorized_action('DELETE', "api/senders/{$sender->getKey()}");
    }

    public function test_cant_delete_personal_account()
    {
        $sender = $this->createSenderForRequest(true, true);

        $this->json('DELETE', "api/senders/{$sender->getKey()}")
            ->assertStatus(422);
    }

    public function test_resource_need_sender_ability()
    {
        $this->resource_need_ability("api/senders", 'sender');
    }
}
