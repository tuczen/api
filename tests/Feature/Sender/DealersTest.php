<?php


namespace Tests\Feature\Sender;


use App\Models\Group;
use App\Models\User;
use Tests\TestCase;

class DealersTest extends TestCase
{
    public function test_any_user_can_list()
    {
        $count = rand(3, 5);
        $group = Group::factory()->create();

        User::factory()->dealer()
            ->attachGroup($group)
            ->count($count)
            ->create();

        $this->setSenderUser();

        $this->test_listing("api/groups/{$group->getKey()}/dealers", $count);
    }

    public function test_any_user_can_show()
    {
        $group = Group::factory()->create();
        $dealer = User::factory()
            ->dealer()
            ->attachGroup($group)
            ->create();

        $this->setSenderUser();

        $this->test_show("api/groups/{$group->getKey()}/dealers/{$dealer->getKey()}", $dealer->getKey());
    }

    public function test_sender_admin_can_attach()
    {
        $dealer = User::factory()->dealer()->create();
        $group = Group::factory()->create();
        $senderAdmin = User::factory()
            ->sender()
            ->attachSender($group->owner, true)
            ->create();

        $this->setSenderUser($senderAdmin);

        $this->json('POST', "api/groups/{$group->getKey()}/dealers/", ['id' => $dealer->getKey()])
            ->assertStatus(201);
    }

    public function test_only_sender_admin_can_attach()
    {
        $dealer = User::factory()->dealer()->create();
        $group = Group::factory()->create();
        $senderAdmin = User::factory()
            ->sender()
            ->attachSender($group->owner)
            ->create();

        $this->setSenderUser($senderAdmin);

        $this->test_unauthorized_action(
            'POST', "api/groups/{$group->getKey()}/dealers/", ['id' => $dealer->getKey()]
        );
    }

    public function non_sender_member_user_cant_attach()
    {
        $dealer = User::factory()->dealer()->create();
        $group = Group::factory()->create();

        $this->setSenderUser();

        $this->test_unauthorized_action(
            'POST', "api/groups/{$group->getKey()}/dealers/", ['id' => $dealer->getKey()]
        );
    }

    public function test_sender_admin_can_detach()
    {
        $group = Group::factory()->create();
        $dealer = User::factory()
            ->attachGroup($group)->dealer()
            ->create();

        $senderAdmin = User::factory()
            ->sender()
            ->attachSender($group->owner, true)
            ->create();

        $this->setSenderUser($senderAdmin);

        $this->test_delete("api/groups/{$group->getKey()}/dealers/".$dealer->getKey());
    }

    public function test_only_sender_admin_can_detach()
    {
        $group = Group::factory()->create();
        $dealer = User::factory()
            ->attachGroup($group)->dealer()
            ->create();

        $senderUser = User::factory()
            ->sender()
            ->attachSender($group->owner)
            ->create();

        $this->setSenderUser($senderUser);

        $this->test_unauthorized_action(
            'DELETE', "api/groups/{$group->getKey()}/dealers/".$dealer->getKey()
        );
    }

    public function non_sender_member_user_cant_detach()
    {
        $group = Group::factory()->create();
        $dealer = User::factory()
            ->attachGroup($group)->dealer()
            ->create();

        $this->setSenderUser();

        $this->test_unauthorized_action(
            'POST', "api/groups/{$group->getKey()}/dealers/", ['id' => $dealer->getKey()]
        );
    }

    public function test_try_detach_inexistent()
    {
        $group = Group::factory()->create();
        $dealer = User::factory()->dealer()->create();
        $senderAdmin = User::factory()
            ->sender()
            ->attachSender($group->owner, true)
            ->create();

        $this->setSenderUser($senderAdmin);

        $this->json('DELETE', "api/groups/{$group->getKey()}/dealers/".$dealer->getKey())
            ->assertStatus(404);
    }

    public function test_resource_protection()
    {
        $this->resource_need_authentication("api/groups/GROUPID/dealers", ['update']);
    }

    public function test_resource_need_sender_ability()
    {
        $this->resource_need_ability("api/groups/GROUPID/dealers", 'sender', ['update']);
    }
}
