<?php


namespace Tests\Feature\Sender;


use App\Models\User;
use Tests\CreatesSender;
use Tests\TestCase;

class UserTest extends TestCase
{
    use CreatesSender;

    public function test_admin_user_can_list()
    {
        $sender = $this->createSenderForRequest(true);
        $count = rand(3, 5);

        User::factory()->attachSender($sender)
            ->count($count)->create();

        $this->test_listing( "api/senders/{$sender->getKey()}/users", $count+1);
    }

    public function test_non_admin_user_cant_list()
    {
        $sender = $this->createSenderForRequest(false);

        $this->test_unauthorized_action('GET', "api/senders/{$sender->getKey()}/users");
    }

    public function test_admin_user_can_show()
    {
        $sender = $this->createSenderForRequest(true);
        $userId = $sender->users()->first()->getKey();

        $this->test_show("api/senders/{$sender->getKey()}/users", $userId);
    }

    public function test_non_admin_user_cant_show()
    {
        $sender = $this->createSenderForRequest(false);
        $userId = $sender->users()->first()->getKey();

        $this->test_unauthorized_action('GET', "api/senders/{$sender->getKey()}/users/$userId");
    }

    public function test_admin_user_can_update()
    {
        $sender = $this->createSenderForRequest(true);
        $userId = $sender->users()->first()->getKey();

        $this->test_update("api/senders/{$sender->getKey()}/users/$userId", ['is_admin' => false]);
    }

    public function test_invalid_update_request()
    {
        $sender = $this->createSenderForRequest(true);
        $userId = $sender->users()->first()->getKey();

        $this->json('PUT', "api/senders/{$sender->getKey()}/users/$userId")
            ->assertJsonValidationErrors(['is_admin']);
    }

    public function test_non_admin_user_cant_update()
    {
        $sender = $this->createSenderForRequest(false);
        $userId = $sender->users()->first()->getKey();

        $this->test_unauthorized_action(
            'PUT', "api/senders/{$sender->getKey()}/users/$userId", ['is_admin' => true]
        );
    }

    public function test_admin_user_can_attach()
    {
        $sender = $this->createSenderForRequest(true);
        $data = [
            'id' => User::factory()->create()->getKey(),
            'is_admin' => false
        ];

        $this->test_creation("api/senders/{$sender->getKey()}/users", $data);
    }

    public function test_non_admin_user_cant_attach()
    {
        $sender = $this->createSenderForRequest(false);
        $data = [
            'id' => User::factory()->create()->getKey(),
            'is_admin' => false
        ];

        $this->test_unauthorized_action(
            'POST', "api/senders/{$sender->getKey()}/users", $data
        );
    }

    public function test_invalid_attach_request()
    {
        $sender = $this->createSenderForRequest(true);

        $this->json('POST', "api/senders/{$sender->getKey()}/users")
            ->assertJsonValidationErrors(['is_admin', 'id']);
    }

    public function test_admin_user_can_delete()
    {
        $sender = $this->createSenderForRequest(true);
        $userId = $sender->users()->first()->getKey();

        $this->test_delete("api/senders/{$sender->getKey()}/users/$userId");
    }

    public function test_any_user_cant_delete()
    {
        $sender = $this->createSenderForRequest(false);
        $userId = $sender->users()->first()->getKey();

        $this->test_unauthorized_action(
            'DELETE', "api/senders/{$sender->getKey()}/users/$userId"
        );
    }

    public function test_resource_protection()
    {
        $this->resource_need_authentication("api/senders/SENDERID/users");
    }

    public function test_resource_need_sender_ability()
    {
        $this->resource_need_ability("api/senders/SENDERID/users", 'sender');
    }
}
