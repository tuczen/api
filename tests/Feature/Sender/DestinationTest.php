<?php


namespace Tests\Feature\Sender;


use App\Models\Destination;
use App\Models\Shipment;
use Faker\Generator;
use Illuminate\Testing\TestResponse;
use Tests\TestCase;

class DestinationTest extends TestCase
{
    public function test_sender_user_can_list()
    {
        $count = rand(3, 5);
        $shipment = Shipment::factory()->create();
        Destination::factory()->for($shipment)
            ->count($count)->create();

        $this->setSenderUser($shipment->creator);

        $this->test_listing("api/shipments/{$shipment->getKey()}/destinations", $count);
    }

    public function test_only_sender_user_can_list()
    {
        $count = rand(3, 5);
        $shipment = Shipment::factory()->create();
        Destination::factory()->for($shipment)
            ->count($count)->create();

        $this->setSenderUser();

        $this->test_unauthorized_action('GET', "api/shipments/{$shipment->getKey()}/destinations");
    }

    public function test_sender_user_can_show()
    {
        $destination = Destination::factory()->create();

        $this->setSenderUser($destination->shipment->creator);

        $this->test_show($this->makeUriForDestination($destination), $destination->getKey());
    }

    public function test_only_sender_user_can_show()
    {
        $destination = Destination::factory()->create();

        $this->setSenderUser();

        $this->test_unauthorized_action('GET', $this->makeUriForDestination($destination));
    }

    public function test_sender_user_can_update()
    {
        $destination = Destination::factory()->create();

        $this->setSenderUser($destination->shipment->creator);

        $data = Destination::factory()->make()->toArray();

        $this->test_update($this->makeUriForDestination($destination), $data);
    }

    public function test_invalid_amount_format_update()
    {
        $this->test_update_validation(['amount' => '1122334455A']);
    }

    public function test_invalid_contact_phone_format_update()
    {
        $this->test_update_validation(['contact_phone' => '1122334455A']);
    }

    public function test_invalid_date_from_format_update()
    {
        $this->test_update_validation(['date_from' => now()->addHour()->format('Y-m-d')]);
    }

    public function test_invalid_date_to_format_update()
    {
        $this->test_update_validation(['date_to' => now()->addHour()->format('Y-m-d')]);
    }

    public function test_date_to_is_required_when_date_from_is_present_update()
    {
        $this->test_update_validation([
            'date_from' => now()->addHour()->format('Y-m-d H:i'),
            'date_to' => ''
        ], ['date_to']);
    }

    public function test_date_from_is_required_when_date_to_is_present_update()
    {
        $this->test_update_validation([
            'date_from' => '',
            'date_to' => now()->addHour()->format('Y-m-d H:i')
        ], ['date_from']);
    }

    public function test_date_from_must_be_a_date_before_date_to_update()
    {
        $this->test_update_validation([
            'date_from' => now()->addHour()->format('Y-m-d H:i'),
            'date_to' => now()->addMinute()->format('Y-m-d H:i')
        ], ['date_from']);
    }

    public function test_date_from_must_be_a_future_date_update()
    {
        $this->test_update_validation([
            'date_from' => now()->subHour()->format('Y-m-d H:i'),
            'date_to' => now()->addHour()->format('Y-m-d H:i')
        ], ['date_from']);
    }

    public function test_invalid_message_size_update()
    {
        $message = app(Generator::class)
            ->realTextBetween(256, 260);

        $this->test_update_validation(compact('message'));
    }

    public function test_invalid_amount_format_store()
    {
        $this->test_store_validation(['amount' => '1122334455A']);
    }

    public function test_invalid_contact_phone_format_store()
    {
        $this->test_store_validation(['contact_phone' => '1122334455A']);
    }

    public function test_invalid_date_from_format_store()
    {
        $this->test_store_validation(['date_from' => now()->addHour()->format('Y-m-d')]);
    }

    public function test_invalid_date_to_format_store()
    {
        $this->test_store_validation(['date_to' => now()->addHour()->format('Y-m-d')]);
    }

    public function test_date_to_is_required_when_date_from_is_present_store()
    {
        $this->test_store_validation([
            'date_from' => now()->addHour()->format('Y-m-d H:i'),
            'date_to' => ''
        ], ['date_to']);
    }

    public function test_date_from_is_required_when_date_to_is_present_store()
    {
        $this->test_store_validation([
            'date_from' => '',
            'date_to' => now()->addHour()->format('Y-m-d H:i')
        ], ['date_from']);
    }

    public function test_date_from_must_be_a_date_before_date_to_store()
    {
        $this->test_store_validation([
            'date_from' => now()->addHour()->format('Y-m-d H:i'),
            'date_to' => now()->addMinute()->format('Y-m-d H:i')
        ], ['date_from']);
    }

    public function test_date_from_must_be_a_future_date_store()
    {
        $this->test_store_validation([
            'date_from' => now()->subHour()->format('Y-m-d H:i'),
            'date_to' => now()->addHour()->format('Y-m-d H:i')
        ], ['date_from']);
    }

    public function test_invalid_message_size_store()
    {
        $message = app(Generator::class)
            ->realTextBetween(256, 260);

        $this->test_store_validation(compact('message'));
    }

    /**
     * @param  array $invalidData
     * @param  array $errors
     * @return TestResponse
     */
    protected function test_update_validation(array $invalidData, array $errors = []): TestResponse
    {
        return $this->test_update_or_create_validation('PUT', $invalidData, $errors);
    }

    /**
     * @param  string $method
     * @param  array $invalidData
     * @param  array $errors
     * @return TestResponse
     */
    protected function test_update_or_create_validation(string $method, array $invalidData, array $errors = []): TestResponse
    {
        $destination = Destination::factory()->create();

        $this->setSenderUser($destination->shipment->creator);

        $data = array_merge(Destination::factory()->make()->toArray(), $invalidData);
        $errors = $errors ?: array_keys($invalidData);

        return $this->json($method, $this->makeUriForDestination($destination, $method == 'PUT'), $data)
            ->assertJsonValidationErrors($errors);
    }

    /**
     * @param  array $invalidData
     * @param  array $errors
     * @return TestResponse
     */
    protected function test_store_validation(array $invalidData, array $errors = []): TestResponse
    {
        $data = array_merge(Destination::factory()->make()->toArray(), $invalidData);
        $errors = $errors ?: array_keys($invalidData);

        return $this->test_update_or_create_validation('POST', $data, $errors);
    }

    /**
     * @param  Destination $destination
     * @param  bool $withId
     * @return string
     */
    protected function makeUriForDestination(Destination $destination, bool $withId = true): string
    {
        return "api/shipments/{$destination->shipment->getKey()}/destinations"
            .($withId ? "/{$destination->getKey()}" : "");
    }

    public function test_resource_protection()
    {
        $this->resource_need_authentication("api/shipments/SHIPMENTID/destinations");
    }

    public function test_resource_need_sender_ability()
    {
        $this->resource_need_ability("api/shipments/SHIPMENTID/destinations", 'sender');
    }
}
