<?php


namespace Tests\Feature\Sender;


use App\Models\Shipment;
use App\Models\User;
use Database\Factories\ShipmentFactory;
use Tests\TestCase;

class ShipmentTest extends TestCase
{

    public function test_sender_user_can_list()
    {
        $count = rand(3, 5);
        $shipment = $this->createShipment();
        Shipment::factory()->withSender($shipment->sender)->count($count)->create();

        $this->test_listing($this->makeUri($shipment, false), $count+1);
    }

    public function test_any_user_cant_list()
    {
        $uri = $this->makeUri(null, false);
        $this->setSenderUser();

        $this->test_unauthorized_action('GET', $uri);
    }

    public function test_sender_user_can_show()
    {
        $shipment = $this->createShipment();

        $this->test_show(
            $this->makeUri($shipment), $shipment->getKey()
        );
    }

    public function test_any_user_cant_show()
    {
        $uri = $this->makeUri();
        $this->setSenderUser();

        $this->test_unauthorized_action('GET', $uri);
    }

    public function test_sender_user_can_update_name()
    {
        $this->test_update($this->makeUri(), ['name' => __FUNCTION__]);
    }

    public function test_invalid_update_request()
    {
        $this->json('PUT', $this->makeUri())
            ->assertJsonValidationErrors(['name']);
    }

    public function test_sender_user_can_confirm()
    {
        $factory = Shipment::factory()
            ->assigned()
            ->withDestinations();

        $shipment = $this->createShipment($factory);

        $this->test_update($this->makeUri($shipment), ['status' => 'confirmed']);
    }

    public function test_sender_user_cant_confirm_without_dealer()
    {
        $factory = Shipment::factory()->withDestinations();

        $this->test_update_status_validation($factory);
    }

    public function test_sender_user_cant_confirm_without_destinations()
    {
        $factory = Shipment::factory()->assigned();

        $this->test_update_status_validation($factory);
    }

    public function test_sender_user_cant_update_confirmed()
    {
        $factory = Shipment::factory()
            ->assigned()
            ->confirmed()
            ->withDestinations();

        $this->test_update_status_validation($factory);
    }

    public function test_sender_user_can_create_without_dealer()
    {
        $shipment = $this->createShipment();
        $data = [
            'name' => uniqid(),
            'amount' => rand(-5000, 5000),
        ];

        $this->test_creation($this->makeUri($shipment, false), $data);

        $this->assertDatabaseHas((new Shipment)->getTable(), [
            'name' => $data['name'],
            'creator_id' => $shipment->creator->getKey()
        ]);
    }

    public function test_sender_user_can_create_with_dealer()
    {
        $shipment = $this->createShipment();

        $data = [
            'name' => uniqid(),
            'amount' => rand(-5000, 5000),
            'dealer_id' => User::factory()->dealer()->create()->getKey()
        ];

        $this->test_creation($this->makeUri($shipment, false), $data);

        $this->assertDatabaseHas((new Shipment)->getTable(), [
            'name' => $data['name'],
            'creator_id' => $shipment->creator->getKey()
        ]);
    }

    public function test_sender_user_cant_create_with_inexistent_dealer()
    {
        $data = [
            'name' => Shipment::factory()->make()->name,
            'dealer_id' => rand(1000, 9999)
        ];

        $this->json('POST', $this->makeUri(null, false), $data)
            ->assertJsonValidationErrors(['dealer_id']);
    }

    public function test_sender_user_can_delete()
    {
        $shipment = $this->createShipment();

        $this->test_delete($this->makeUri($shipment));
    }

    public function test_any_user_cant_delete()
    {
        $shipment = $this->createShipment();
        $uri = $this->makeUri($shipment);
        $this->setSenderUser();

        $this->test_unauthorized_action('GET', $uri);
    }

    protected function test_update_status_validation(ShipmentFactory $shipmentFactory)
    {
        $shipment = $this->createShipment($shipmentFactory);

        $this->json('PUT', $this->makeUri($shipment), ['status' => 'confirmed'])
            ->assertJsonValidationErrors(['status']);
    }

    /**
     * @param  ShipmentFactory|null $shipmentFactory
     * @return Shipment
     */
    protected function createShipment(?ShipmentFactory $shipmentFactory = null): Shipment
    {
        $shipmentFactory = $shipmentFactory ?: Shipment::factory();

        return $shipmentFactory->create();
    }

    /**
     * @param  Shipment|null $shipment
     * @param  bool $withId
     * @return string
     */
    protected function makeUri(?Shipment $shipment = null, bool $withId = true): string
    {
        $shipment = $shipment ?: $this->createShipment();

        $this->setSenderUser($shipment->creator);

        return "api/senders/{$shipment->sender->getKey()}/shipments".($withId ? "/{$shipment->getKey()}" : "");
    }

    public function test_resource_protection()
    {
        $this->resource_need_authentication("api/senders/SENDERID/shipments");
    }

    public function test_resource_need_sender_ability()
    {
        $this->resource_need_ability("api/senders/SENDERID/shipments", 'sender');
    }
}
