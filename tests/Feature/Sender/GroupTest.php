<?php


namespace Tests\Feature\Sender;


use App\Models\Group;
use App\Models\Sender;
use App\Models\User;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Sanctum\Sanctum;
use Tests\CreatesSender;
use Tests\TestCase;

class GroupTest extends TestCase
{
    use CreatesSender;

    public function test_any_user_can_list()
    {
        $count = rand(3, 5);
        $sender = $this->createSenderForRequest(false);
        Group::factory()->for($sender, 'owner')->count($count)->create();

        $this->test_listing("api/senders/{$sender->getKey()}/groups", $count);
    }

    public function test_any_user_can_show()
    {
        $sender = Sender::factory()->create();
        $groupId = Group::factory()->for($sender, 'owner')->create()->getKey();

        Sanctum::actingAs(User::factory()->sender()->make(), ['sender']);

        $this->test_show("api/senders/{$sender->getKey()}/groups", $groupId);
    }

    public function test_admin_user_can_update()
    {
        $sender = $this->createSenderForRequest(true);
        $groupId = Group::factory()->for($sender, 'owner')->create()->getKey();

        $this->test_update("api/senders/{$sender->getKey()}/groups/$groupId", ['name' => __FUNCTION__]);
    }

    public function test_invalid_update_request()
    {
        $sender = $this->createSenderForRequest(true);
        $groupId = Group::factory()->for($sender, 'owner')->create()->getKey();

        $this->json('PUT', "api/senders/{$sender->getKey()}/groups/$groupId")
            ->assertJsonValidationErrors(['name']);
    }

    public function test_non_admin_user_cant_update()
    {
        $sender = $this->createSenderForRequest(false);
        $groupId = Group::factory()->for($sender, 'owner')->create()->getKey();

        $this->test_unauthorized_action(
            'PUT', "api/senders/{$sender->getKey()}/groups/$groupId", ['name' => __FUNCTION__]
        );
    }

    public function test_admin_user_can_create()
    {
        $sender = $this->createSenderForRequest(true);
        $data = [
            'name' => __FUNCTION__
        ];

        $this->test_creation("api/senders/{$sender->getKey()}/groups", $data);
    }

    public function test_non_admin_user_cant_create()
    {
        $sender = $this->createSenderForRequest(false);
        $data = [
            'name' => __FUNCTION__
        ];

        $this->test_unauthorized_action(
            'POST', "api/senders/{$sender->getKey()}/groups", $data
        );
    }

    public function test_invalid_create_request()
    {
        $sender = $this->createSenderForRequest(true);

        $this->json('POST', "api/senders/{$sender->getKey()}/groups")
            ->assertJsonValidationErrors(['name']);
    }

    public function test_admin_user_can_delete()
    {
        $sender = $this->createSenderForRequest(true);
        $groupId = Group::factory()->for($sender, 'owner')->create()->getKey();

        $this->test_delete("api/senders/{$sender->getKey()}/groups/$groupId");
    }

    public function test_any_user_cant_delete()
    {
        $sender = $this->createSenderForRequest(false);
        $groupId = Group::factory()->for($sender, 'owner')->create()->getKey();

        $this->test_unauthorized_action(
            'DELETE', "api/senders/{$sender->getKey()}/groups/$groupId"
        );
    }

    public function test_resource_protection()
    {
        $this->resource_need_authentication("api/senders/SENDERID/groups");
    }

    public function test_resource_need_sender_ability()
    {
        $this->resource_need_ability("api/senders/SENDERID/groups", 'sender');
    }
}
