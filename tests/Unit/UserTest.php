<?php


namespace Tests\Unit;


use App\Models\User;
use Tests\TestCase;

class UserTest extends TestCase
{

    public function test_new_user_has_personal_account()
    {
        $user = User::factory()->create();

        $this->assertEquals(1, $user->senders()->where('personal_account', true)->count());
    }
}
