<?php


namespace Tests;


use App\Models\Sender;
use App\Models\User;
use Laravel\Sanctum\Sanctum;

trait CreatesSender
{
    /**
     * @param  bool $asAdminUser
     * @param  bool $personalAccount
     * @return Sender
     */
    protected function createSenderForRequest(bool $asAdminUser = false, bool $personalAccount = false): Sender
    {
        $user = User::factory()->sender()->create();
        Sanctum::actingAs($user, ['sender']);

        if ($personalAccount) {
            return $user->senders()->first();
        }

        return Sender::factory()
            ->withUser($user, ['is_admin' => $asAdminUser])
            ->create();
    }
}
