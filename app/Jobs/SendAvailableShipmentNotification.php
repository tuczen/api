<?php

namespace App\Jobs;

use App\Models\Group;
use App\Models\Shipment;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendAvailableShipmentNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Shipment
     */
    protected Shipment $shipment;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Shipment $shipment)
    {
        $this->shipment = $shipment;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $notification = $this->makeNotification();

        foreach ($this->shipment->groups as $group) {

            fcm()
                ->to($this->groupDeviceTokens($group))
                ->notification($notification)
                ->send();
        }
    }

    /**
     * @return array
     */
    protected function makeNotification(): array
    {
        $title = "Nuevo envío de {$this->shipment->sender->name}";
        $body = "{$this->shipment->name} x $ {$this->shipment->amount}";

        return compact('title', 'body');
    }

    /**
     * @param  Group $group
     * @return array
     */
    protected function groupDeviceTokens(Group $group): array
    {
        return $group->dealers()
            ->whereNotNull('device_token')
            ->get()
            ->pluck('device_token')
            ->toArray();
    }
}
