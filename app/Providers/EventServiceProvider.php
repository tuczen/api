<?php

namespace App\Providers;

use App\Models\Group;
use App\Models\Shipment;
use App\Models\User;
use App\Observers\GroupObserver;
use App\Observers\ShipmentObserver;
use App\Observers\UserObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        User::observe(UserObserver::class);
        Group::observe(GroupObserver::class);
        Shipment::observe(ShipmentObserver::class);
    }
}
