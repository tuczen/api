<?php

namespace App\Providers;

use App\Models\Group;
use App\Models\Sender;
use App\Models\Shipment;
use App\Policies\GroupPolicy;
use App\Policies\SenderPolicy;
use App\Policies\ShipmentPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Sender::class => SenderPolicy::class,
        Group::class => GroupPolicy::class,
        Shipment::class => ShipmentPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
