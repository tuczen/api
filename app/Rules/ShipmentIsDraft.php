<?php

namespace App\Rules;

use App\Models\Shipment;
use Illuminate\Contracts\Validation\Rule;

class ShipmentIsDraft implements Rule
{
    /**
     * @var Shipment
     */
    protected Shipment $shipment;

    /**
     * ShipmentIsDraft constructor.
     * @param  Shipment $shipment
     */
    public function __construct(Shipment $shipment)
    {
        $this->shipment = $shipment;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        return $this->shipment->status == 'draft';
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return 'Shipment cannot be confirmed.';
    }
}
