<?php

namespace App\Rules;

use App\Models\Shipment;
use Illuminate\Contracts\Validation\Rule;

class ShipmentHasDestination implements Rule
{
    /**
     * @var Shipment
     */
    protected Shipment $shipment;

    /**
     * ShipmentHasDestination constructor.
     * @param  Shipment $shipment
     */
    public function __construct(Shipment $shipment)
    {
        $this->shipment = $shipment;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return (bool) $this->shipment->destinations()->first();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'A destination must be added before confirm.';
    }
}
