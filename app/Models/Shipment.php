<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Database\Query\JoinClause;

class Shipment extends Model
{
    use HasFactory;

    /**
     * @var string[]
     */
    protected $fillable = ['name', 'creator_id', 'dealer_id', 'amount'];

    /**
     * @return BelongsTo
     */
    public function sender(): BelongsTo
    {
        return $this->belongsTo(Sender::class);
    }

    /**
     * @return HasMany
     */
    public function destinations(): HasMany
    {
        return $this->hasMany(Destination::class);
    }

    /**
     * @return HasOne
     */
    public function dealer(): HasOne
    {
        return $this->hasOne(User::class);
    }

    /**
     * @return BelongsTo
     */
    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return BelongsToMany
     */
    public function groups(): BelongsToMany
    {
        return $this->belongsToMany(Group::class);
    }

    public function scopeAvailableForUser(Builder $builder, User $user)
    {
        return $builder->whereIn($this->getKeyName(), function (QueryBuilder $query) use ($user) {

           $query->select('group_shipment.shipment_id')
               ->distinct()
               ->from('group_shipment')
               ->join('group_user', function (JoinClause $join) use ($user) {

                   $join->on('group_shipment.group_id', '=', 'group_user.group_id')
                       ->where('group_user.user_id', $user->getKey());
               });
        });
    }
}
