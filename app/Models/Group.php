<?php


namespace App\Models;


use App\Models\User as Dealer;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Group extends Model
{
    use HasFactory;

    /**
     * @var string[]
     */
    protected $fillable = ['name'];
    /**
     * @var string[]
     */
    protected $hidden = ['sender_id', 'group_id'];

    /**
     * @return BelongsToMany
     */
    public function dealers(): BelongsToMany
    {
        return $this->belongsToMany(Dealer::class);
    }

    /**
     * @return BelongsTo
     */
    public function owner(): BelongsTo
    {
        return $this->belongsTo(Sender::class);
    }

    /**
     * @return BelongsToMany
     */
    public function senders(): BelongsToMany
    {
        return $this->belongsToMany(Sender::class);
    }
}
