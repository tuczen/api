<?php

namespace App\Http\Middleware;


use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TokenAbility
{
    /**
     * Handle an incoming request.
     *
     * @param  Request $request
     * @param  Closure $next
     * @param  string $ability
     * @return mixed
     */
    public function handle(Request $request, Closure $next, string $ability)
    {
        if (! $request->user()->tokenCan($ability)) {
            return response()->json([
                'message' => "Required ability [$ability] is missing."
            ], Response::HTTP_FORBIDDEN);
        }

        return $next($request);
    }
}
