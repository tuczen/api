<?php


namespace App\Http\Controllers;


use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function user(): JsonResponse
    {
        return response()->json(auth()->user());
    }

    /**
     * @return JsonResponse
     */
    public function senders(): JsonResponse
    {
        $senders = auth()->user()
            ->senders;

        return response()->json($senders);
    }

    /**
     * @param  Request $request
     * @return JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function setToken(Request $request): JsonResponse
    {
        $this->validate($request, [
            'device_token' => 'required'
        ]);

        /** @var User $user */
        $user = auth()->user();
        $user->device_token = $request->input('device_token');
        $user->save();

        return response()->json();
    }
}
