<?php

namespace App\Http\Controllers;

use App\Utils\SqlParser;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @param  Builder $builder
     * @param  array|null $keysFilter
     * @return Builder
     * @throws Exception
     */
    protected function makeIndexBuilder(Builder $builder, array $keysFilter = null): Builder
    {
        $request = request();
        $keys = $keysFilter ? array_values(end($keysFilter)) : [];

        $params = isset($keysFilter['only'])
            ? $request->only($keys)
            : $request->except(array_merge(
                ['page', 'page_size'], $keys
            ));

        return SqlParser::parse($builder, $params);
    }

    /**
     * @param  Builder $builder
     * @param  array $keysFilter
     * @return JsonResponse
     * @throws Exception
     */
    protected function indexResource(Builder $builder, array $keysFilter = []): JsonResponse
    {
        $perPage = request()->input('page_size');
        $results = $this->makeIndexBuilder($builder, $keysFilter)
            ->paginate($perPage);

        return response()->json($results);
    }
}
