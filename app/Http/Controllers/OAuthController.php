<?php


namespace App\Http\Controllers;


use App\Models\Sender;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Laravel\Socialite\Facades\Socialite;
use \Laravel\Socialite\Contracts\User as GUser;

class OAuthController extends Controller
{
    const SOCIALITE_PROVIDER = "google";

    /**
     * @param  Request $request
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function redirect(Request $request): RedirectResponse
    {
        $this->validate($request, [
            'user_type' => ['required', Rule::in(['dealer', 'sender'])],
            'device_name' => 'required'
        ]);

        $state = json_encode($request->all());

        return Socialite::driver(self::SOCIALITE_PROVIDER)
            ->with(compact('state'))
            ->stateless()
            ->redirect();
    }

    /**
     * @param  Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function callback(Request $request): JsonResponse
    {
        $this->validate($request, [
            'state' => 'required|json',
        ]);

        $state = json_decode($request->state);

        $gUser = Socialite::driver(self::SOCIALITE_PROVIDER)
            ->stateless()->user();

        $user = User::where('email', $gUser->getEmail())->first()
            ?: $this->createUser($gUser, $state->user_type == 'dealer');

        $token = $user->createToken($state->device_name, [$state->user_type])
            ->plainTextToken;

        return response()->json(compact('token'));
    }

    /**
     * @param  GUser $gUser
     * @param  bool $isDealer
     * @return User
     */
    protected function createUser(GUser $gUser, bool $isDealer): User
    {
        $user = new User();
        $user->name = $gUser->getName();
        $user->email = $gUser->getEmail();
        $user->password = bcrypt(Str::random());
        $user->is_dealer = $isDealer;
        $user->save();

        return $user;
    }
}
