<?php


namespace App\Http\Controllers\Senders;


use App\Models\Group;
use App\Models\User as Dealer;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class DealerController extends Controller
{
    /**
     * UserController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->middleware('can:admin,group')
            ->except('index', 'show');
    }

    /**
     * @param  Group $group
     * @return JsonResponse
     * @throws Exception
     */
    public function index(Group $group): JsonResponse
    {
        return $this->indexResource($group->dealers()->getQuery());
    }

    /**
     * @param  Group $group
     * @param  Dealer $dealer
     * @return JsonResponse
     */
    public function show(Group $group, Dealer $dealer): JsonResponse
    {
        return response()->json($dealer);
    }

    /**
     * @param  Group $group
     * @param  Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store(Group $group, Request $request): JsonResponse
    {
        $this->validate($request, [
           'id' => [
               'required',
               Rule::exists((new Dealer)->getTable())
                   ->where('id', $request->input('id'))
                   ->where('is_dealer', 1)
           ]
        ]);

        $group->dealers()->syncWithoutDetaching($request->input('id'));

        return response()->json([], Response::HTTP_CREATED);
    }

    /**
     * @param  Group $group
     * @param  Dealer $dealer
     * @return JsonResponse
     */
    public function destroy(Group $group, Dealer $dealer): JsonResponse
    {
        $group->dealers()->detach($dealer->getKey());

        return response()->json();
    }
}
