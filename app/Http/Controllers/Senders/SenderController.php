<?php


namespace App\Http\Controllers\Senders;


use App\Models\Sender;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

class SenderController extends Controller
{
    /**
     * SenderController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->middleware('can:admin,sender')
            ->except('index', 'show', 'store');
    }

    /**
     * @return JsonResponse
     * @throws Exception
     */
    public function index(): JsonResponse
    {
        return $this->indexResource(Sender::query());
    }

    /**
     * @param  Sender $sender
     * @return JsonResponse
     */
    public function show(Sender $sender): JsonResponse
    {
        return response()->json($sender);
    }

    /**
     * @param  Sender $sender
     * @param  Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function update(Sender $sender, Request $request): JsonResponse
    {
        $this->validate($request, [
           'name' => 'required'
        ]);

        $sender->name = $request->input('name');
        $sender->save();

        return response()->json();
    }

    /**
     * @param  Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store(Request $request): JsonResponse
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $sender = new Sender();
        $sender->name = $request->input('name');
        $sender->save();

        $sender->users()
            ->attach($request->user()->id, ['is_admin' => true]);

        return response()->json([], Response::HTTP_CREATED);
    }

    /**
     * @param  Sender $sender
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(Sender $sender): JsonResponse
    {
        if ($sender->personal_account) {
            return response()->json([
                "message" => "Can't delete personal account.",
                "errors" => []
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $sender->delete();

        return response()->json();
    }
}
