<?php


namespace App\Http\Controllers\Senders;


use App\Models\User as Dealer;
use App\Models\Sender;
use App\Models\Shipment;
use App\Rules\ShipmentHasDealer;
use App\Rules\ShipmentHasDestination;
use App\Rules\ShipmentIsDraft;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class ShipmentController extends Controller
{

    /**
     * ShipmentController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('can:adminShipments,sender');
    }

    /**
     * @param  Sender $sender
     * @return JsonResponse
     * @throws Exception
     */
    public function index(Sender $sender): JsonResponse
    {
        return $this->indexResource($sender->shipments()->getQuery());
    }

    /**
     * @param  Sender $sender
     * @param  Shipment $shipment
     * @return JsonResponse
     */
    public function show(Sender $sender, Shipment $shipment): JsonResponse
    {
        return response()->json($shipment);
    }

    /**
     * @param  Sender $sender
     * @param  Shipment $shipment
     * @param  Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function update(Sender $sender, Shipment $shipment, Request $request): JsonResponse
    {
        $this->validate($request, [
            'name' => 'required_without:status',
            'status' => [
                Rule::in('confirmed'),
                new ShipmentIsDraft($shipment),
                new ShipmentHasDealer($shipment),
                new ShipmentHasDestination($shipment),
            ]
        ]);

        if ($request->has('name')) {
            $shipment->name = $request->input('name');
        }

        if ($request->has('status')) {
            $shipment->status = $request->input('status');
        }

        $shipment->save();

        return response()->json();
    }

    /**
     * @param  Sender $sender
     * @param  Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store(Sender $sender, Request $request): JsonResponse
    {
        $this->validate($request, [
            'name' => 'required',
            'amount' => 'required|numeric',
            'dealer_id' => 'exists:'.Dealer::class.',id'
        ]);

        $attributes = array_merge(
            $request->only('name', 'dealer_id', 'amount'),
            ['creator_id' => $request->user()->id]
        );

        $sender->shipments()->create($attributes);

        return response()->json([], Response::HTTP_CREATED);
    }

    /**
     * @param  Sender $sender
     * @param  Shipment $shipment
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(Sender $sender, Shipment $shipment): JsonResponse
    {
        $shipment->delete();

        return response()->json();
    }
}
