<?php


namespace App\Http\Controllers\Senders;


use App\Models\Sender;
use App\Models\User;
use App\Utils\SqlParser;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{
    /**
     * UserController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->middleware('can:admin,sender');
    }

    /**
     * @param  Sender $sender
     * @return JsonResponse
     */
    public function index(Sender $sender): JsonResponse
    {
        return $this->indexResource($sender->users()->getQuery());
    }

    /**
     * @param  Sender $sender
     * @param  User $user
     * @return JsonResponse
     */
    public function show(Sender $sender, User $user): JsonResponse
    {
        return response()->json($user);
    }

    /**
     * @param Sender $sender
     * @param User $user
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function update(Sender $sender, User $user, Request $request): JsonResponse
    {
        $this->validate($request, [
            'is_admin' => 'required|boolean'
        ]);

        $sender->users()
            ->updateExistingPivot($user->getKey(), [
                'is_admin' => $request->is_admin
            ]);

        return response()->json([]);
    }

    /**
     * @param  Sender $sender
     * @param  Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store(Sender $sender, Request $request): JsonResponse
    {
        $this->validate($request, [
           'id' => 'required|exists:'.User::class,
           'is_admin' => 'required|boolean'
        ]);

        $sender->users()->attach($request->id, [
            'is_admin' => $request->is_admin
        ]);

        return response()->json([], Response::HTTP_CREATED);
    }

    /**
     * @param  Sender $sender
     * @param  User $user
     * @return JsonResponse
     */
    public function destroy(Sender $sender, User $user): JsonResponse
    {
        $sender->users()->detach($user->getKey());

        return response()->json();
    }
}
