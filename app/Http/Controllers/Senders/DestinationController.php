<?php


namespace App\Http\Controllers\Senders;


use App\Models\Destination;
use App\Models\Shipment;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

class DestinationController extends Controller
{

    /**
     * DestinationController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->middleware('can:admin,shipment');
    }

    /**
     * @param  Shipment $shipment
     * @return JsonResponse
     * @throws Exception
     */
    public function index(Shipment $shipment): JsonResponse
    {
        return $this->indexResource($shipment->destinations()->getQuery());
    }

    /**
     * @param  Shipment $shipment
     * @param  Destination $destination
     * @return JsonResponse
     */
    public function show(Shipment $shipment, Destination $destination): JsonResponse
    {
        return response()->json($destination);
    }

    /**
     * @param  Shipment $shipment
     * @param  Destination $destination
     * @param  Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function update(Shipment $shipment, Destination $destination, Request $request): JsonResponse
    {
        $this->validate($request, [
            'contact_phone' => 'regex:/^[0-9\-\(\)\/\+\.\s]*$/',
            'date_from' => 'required_with:date_to|date_format:Y-m-d H:i|before:date_to|after:'.now(),
            'date_to' => 'required_with:date_from|date_format:Y-m-d H:i',
            'amount' => 'numeric',
            'is_pick_up' => 'boolean',
            'message' => 'max:255',
        ]);

        $destination->update(
            $request->only((new Destination())->getFillable())
        );

        return response()->json();
    }

    /**
     * @param  Shipment $shipment
     * @param  Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store(Shipment $shipment, Request $request): JsonResponse
    {
        $this->validate($request, [
            'address' => 'required',
            'contact_name' => 'required',
            'contact_phone' => 'required|regex:/^[0-9\-\(\)\/\+\.\s]*$/',
            'date_from' => 'required|date_format:Y-m-d H:i|before:date_to|after:'.now(),
            'date_to' => 'required|date_format:Y-m-d H:i',
            'amount' => 'required|numeric',
            'is_pick_up' => 'required|boolean',
            'message' => 'max:255',
        ]);

        $shipment->destinations()
            ->create($request->only((new Destination())->getFillable()));

        return response()->json([], Response::HTTP_CREATED);
    }

    /**
     * @param  Shipment $shipment
     * @param  Destination $destination
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(Shipment $shipment, Destination $destination): JsonResponse
    {
        if ($destination->status !== 'pending') {
            return response()->json([
                "message" => "Only pending destinations can be deleted",
                "errors" => []
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $destination->delete();

        return response()->json();
    }
}
