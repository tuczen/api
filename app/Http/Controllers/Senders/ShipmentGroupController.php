<?php


namespace App\Http\Controllers\Senders;


use App\Models\Group;
use App\Models\Shipment;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class ShipmentGroupController extends Controller
{
    /**
     * ShipmentGroupController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->middleware('can:admin,shipment');
    }

    /**
     * @param  Shipment $shipment
     * @return JsonResponse
     * @throws Exception
     */
    public function index(Shipment $shipment): JsonResponse
    {
        return $this->indexResource($shipment->groups()->getQuery());
    }

    /**
     * @param  Shipment $shipment
     * @param  Group $group
     * @return JsonResponse
     */
    public function show(Shipment $shipment, Group $group): JsonResponse
    {
        return response()->json($group);
    }

    /**
     * @param  Shipment $shipment
     * @param  Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store(Shipment $shipment, Request $request): JsonResponse
    {
        $existsRule = Rule::exists((new Group)->senders()->getTable())
            ->where('sender_id', $shipment->sender_id);

        $this->validate($request, [
            'group_id' => ['required', $existsRule]
        ]);

        $shipment->groups()
            ->syncWithoutDetaching($request->input('group_id'));

        return response()->json([], Response::HTTP_CREATED);
    }

    /**
     * @param  Shipment $shipment
     * @param  Group $group
     * @return JsonResponse
     */
    public function destroy(Shipment $shipment, Group $group): JsonResponse
    {
        $shipment->groups()->detach($group->getKey());

        return response()->json();
    }
}
