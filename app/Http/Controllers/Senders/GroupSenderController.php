<?php


namespace App\Http\Controllers\Senders;


use App\Models\Group;
use App\Models\Sender;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

class GroupSenderController extends Controller
{
    /**
     * @param  Group $group
     * @param  Request $request
     * @return JsonResponse
     * @throws ValidationException
     * @throws AuthorizationException
     */
    public function store(Group $group, Request $request): JsonResponse
    {
        $this->validate($request, [
            'id' => [
                'required',
                'exists:'.(new Sender)->getTable()
            ]
        ]);

        $sender = Sender::find($request->input('id'));
        $this->authorize('admin', $sender);

        $group->senders()
            ->syncWithoutDetaching($request->input('id'));

        return response()->json([], Response::HTTP_CREATED);
    }

    /**
     * @param  Group $group
     * @param  Sender $sender
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function destroy(Group $group, Sender $sender): JsonResponse
    {
        $this->authorize('admin', $sender);

        $group->senders()->detach($sender);

        return response()->json();
    }
}
