<?php


namespace App\Http\Controllers\Senders;


use App\Models\Group;
use App\Models\Sender;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

class GroupController extends Controller
{
    /**
     * SenderController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->middleware('can:admin,sender')
            ->except('index', 'show');
    }

    /**
     * @param  Sender $sender
     * @return JsonResponse
     * @throws Exception
     */
    public function index(Sender $sender): JsonResponse
    {
        return $this->indexResource($sender->groups()->getQuery());
    }

    /**
     * @param  Sender $sender
     * @param  Group $group
     * @return JsonResponse
     */
    public function show(Sender $sender, Group $group): JsonResponse
    {
        return response()->json($group);
    }

    /**
     * @param  Sender $sender
     * @param  Group $group
     * @param  Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function update(Sender $sender, Group $group, Request $request): JsonResponse
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $group->name = $request->name;
        $group->save();

        return response()->json([]);
    }

    /**
     * @param  Sender $sender
     * @param  Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store(Sender $sender, Request $request): JsonResponse
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $sender->ownGroups()->create([
            'name' => $request->name
        ]);

        return response()->json([], Response::HTTP_CREATED);
    }

    /**
     * @param  Sender $sender
     * @param  Group $group
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(Sender $sender, Group $group): JsonResponse
    {
        $group->delete();

        return response()->json();
    }
}
