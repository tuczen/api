<?php


namespace App\Http\Controllers\Dealers;

use App\Http\Controllers\Controller as BaseController;

class Controller extends BaseController
{

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        $this->middleware(['auth:sanctum', 'user_is:dealer']);
    }
}
