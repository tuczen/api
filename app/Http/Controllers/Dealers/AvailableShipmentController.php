<?php


namespace App\Http\Controllers\Dealers;


use App\Models\Shipment;
use Exception;
use Illuminate\Http\JsonResponse;

class AvailableShipmentController extends Controller
{
    /**
     * @return JsonResponse
     * @throws Exception
     */
    public function index(): JsonResponse
    {
        $builder = Shipment::availableForUser(auth()->user());

        return $this->indexResource($builder);
    }

    /**
     * @param  Shipment $shipment
     * @return JsonResponse
     */
    public function update(Shipment $shipment): JsonResponse
    {
        $shipment->dealer_id = auth()->user()->getAuthIdentifier();
        $shipment->save();

        return response()->json();
    }
}
