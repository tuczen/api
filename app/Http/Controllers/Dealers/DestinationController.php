<?php


namespace App\Http\Controllers\Dealers;


use App\Models\Destination;
use App\Models\Shipment;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class DestinationController extends Controller
{
    /**
     * @param  Shipment $shipment
     * @return JsonResponse
     * @throws Exception
     */
    public function index(Shipment $shipment): JsonResponse
    {
        return $this->indexResource($shipment->destinations()->getQuery());
    }

    /**
     * @param  Shipment $shipment
     * @param  Destination $destination
     * @return JsonResponse
     */
    public function show(Shipment $shipment, Destination $destination): JsonResponse
    {
        return response()->json($destination);
    }

    /**
     * @param  Shipment $shipment
     * @param  Destination $destination
     * @param  Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function update(Shipment $shipment, Destination $destination, Request $request): JsonResponse
    {
        $this->validate($request, [
            'status' => Rule::in('in progress', 'finished')
        ]);

        $destination->status = $request->input('status');
        $destination->save();

        return response()->json();
    }
}
