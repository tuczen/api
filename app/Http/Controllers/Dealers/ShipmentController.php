<?php


namespace App\Http\Controllers\Dealers;


use App\Models\Shipment;
use Exception;
use Illuminate\Http\JsonResponse;

class ShipmentController extends Controller
{
    /**
     * @return JsonResponse
     * @throws Exception
     */
    public function index(): JsonResponse
    {
        $builder = auth()->user()
            ->assignedShipments()
            ->getQuery();

        return $this->indexResource($builder);
    }

    /**
     * @param  Shipment $shipment
     * @return JsonResponse
     */
    public function show(Shipment $shipment): JsonResponse
    {
        return response()->json($shipment);
    }
}
