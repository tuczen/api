<?php

namespace App\Observers;


use App\Models\Group;

class GroupObserver
{
    /**
     * Handle the Group "created" event.
     *
     * @param  Group $group
     * @return void
     */
    public function created(Group $group)
    {
        $group->senders()->attach($group->owner);
    }

    /**
     * Handle the Group "deleting" event.
     *
     * @param  Group $group
     * @return void
     */
    public function deleting(Group $group)
    {
        $group->senders()->sync([]);
    }
}
