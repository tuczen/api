<?php


namespace App\Traits;


use App\Models\Sender;
use App\Models\User;

trait SenderAdminPolicy
{
    /**
     * @param  User $user
     * @param  Sender $sender
     * @return bool
     */
    public function admin(User $user, Sender $sender): bool
    {
        return $this->userBelongsToSender($user, $sender, true);
    }

    /**
     * @param  User $user
     * @param  Sender $sender
     * @param  bool|null $isAdminFilter
     * @return bool
     */
    public function userBelongsToSender(User $user, Sender $sender, bool $isAdminFilter = null): bool
    {
        $query = $sender->users()
            ->select($user->getKeyName())
            ->where($user->getKeyName(), $user->getKey());

        if (is_bool($isAdminFilter)) {
            $query->wherePivot('is_admin', true);
        }

        return (bool) $query->first();
    }
}
