<?php

namespace App\Policies;


use App\Models\Group;
use App\Models\User;
use App\Traits\SenderAdminPolicy;
use Illuminate\Auth\Access\HandlesAuthorization;

class GroupPolicy
{
    use HandlesAuthorization, SenderAdminPolicy {admin as senderAdmin;}

    /**
     * @param  User $user
     * @param  Group $group
     * @return bool
     */
    public function admin(User $user, Group $group): bool
    {
        return $this->senderAdmin($user, $group->owner);
    }
}
