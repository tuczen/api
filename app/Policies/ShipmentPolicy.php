<?php


namespace App\Policies;


use App\Models\Shipment;
use App\Models\User;
use App\Traits\SenderAdminPolicy;

class ShipmentPolicy
{
    use SenderAdminPolicy {admin as senderAdmin;}

    /**
     * @param  User $user
     * @param  Shipment $shipment
     * @return bool
     */
    public function admin(User $user, Shipment $shipment): bool
    {
        return $this->userBelongsToSender($user, $shipment->sender);
    }
}
