<?php

namespace App\Policies;


use App\Models\Sender;
use App\Models\User;
use App\Traits\SenderAdminPolicy;
use Illuminate\Auth\Access\HandlesAuthorization;

class SenderPolicy
{
    use HandlesAuthorization, SenderAdminPolicy;

    /**
     * @param User $user
     * @param Sender $sender
     * @return bool
     */
    public function adminShipments(User $user, Sender $sender): bool
    {
        return $this->userBelongsToSender($user, $sender);
    }
}
